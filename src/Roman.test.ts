import Roman from "./Roman";

describe('Roman', () => {
    describe('toArabic()', () => {
        it('XXIII => 23', () => {
            expect(Roman.toArabic('XXIII')).toEqual(23);
        });
        it('XLIII => 53', () => {
            expect(Roman.toArabic('XLIII')).toEqual(43);
        });
        it('MCMLXXVII => 1977', () => {
            expect(Roman.toArabic('MCMLXXVII')).toEqual(1977);
        });
        it('MCMXCIX => 1999', () => {
            expect(Roman.toArabic('MCMXCIX')).toEqual(1999);
        });
        it('CMXLIV => 944', () => {
            expect(Roman.toArabic('CMXLIV')).toEqual(944);
        });
        it('empty string => 0', () => {
            expect(Roman.toArabic('')).toEqual(0);
        });
    });
});
