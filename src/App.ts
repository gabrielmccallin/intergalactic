import * as fs from 'fs';
import ExtractDirectives from "./ExtractDirectives";

const file = fs.readFileSync(process.argv[2] ? process.argv[2] : 'test-input.txt', 'utf-8');
console.log(ExtractDirectives.processFile(file));
