import Translator from "./Translator";
import Model from "./Model";

describe('Translator', () => {
    afterEach(() => {
    });

    describe('elementTotal()', () => {
        it('when glob = I, glob glob Silver returns {Silver, 2}', () => {
            Model.galacticRomanMap = { glob: 'I' };

            const { element, total } = Translator.elementTotal('glob glob Silver');
            expect(element).toEqual('Silver');
            expect(total).toEqual(2);
        });
        it('when parj = X, wort = I, parj wort wort Gold returns {Gold, 13}', () => {
            Model.galacticRomanMap = { parj: 'X', wort: 'I' };
            const { element, total } = Translator.elementTotal('parj wort wort wort Gold');
            expect(element).toEqual('Gold');
            expect(total).toEqual(13);
        });
        it('when parj = L, wort = V, parj wort Hello returns {Hello, 55}', () => {
            Model.galacticRomanMap = { parj: 'L', wort: 'V' };
            const { element, total } = Translator.elementTotal('parj wort Hello');
            expect(element).toEqual('Hello');
            expect(total).toEqual(55);
        });
    });

    describe('galacticToRoman()', () => {
        it('splits glob is I', () => {
            const { galactic, numeral } = Translator.galacticToRoman('glob is I');
            expect(galactic).toEqual('glob');
            expect(numeral).toEqual('I');
        });
        it('splits pish is V', () => {
            const { galactic, numeral } = Translator.galacticToRoman('pish is V');
            expect(galactic).toEqual('pish');
            expect(numeral).toEqual('V');
        });
        it('splits hello is goodbye', () => {
            const { galactic, numeral } = Translator.galacticToRoman('hello is goodbye');
            expect(galactic).toEqual('hello');
            expect(numeral).toEqual('goodbye');
        });
    });

    describe('setValueOfElement()', () => {
        it('glob = I, glob glob Silver is 34 Credits, Silver = 17', () => {
            Model.galacticRomanMap = { glob: 'I' };
            const { element, value } = Translator.setValueOfElement('glob glob Silver is 34 Credits');
            expect(element).toEqual('Silver');
            expect(value).toEqual(17);
        });
        it('asdf = X, nweh = V, asdf nweh Argon is 1500 Credits, Argon = 100', () => {
            Model.galacticRomanMap = { asdf: 'X', nweh: 'V' };
            const { element, value } = Translator.setValueOfElement('asdf nweh Argon is 1500 Credits');
            expect(element).toEqual('Argon');
            expect(value).toEqual(100);
        });
    });

    describe('galacticPhraseToArabic() with glob is I, prok is V, pish is X, tegj is L', () => {
        it('pish tegj glob glob => 42', () => {
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            const arabic = Translator.galacticPhraseToArabic('pish tegj glob glob');
            expect(arabic).toEqual(42);
        });
        it('pish pish glob prok => 24', () => {
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            const arabic = Translator.galacticPhraseToArabic('pish pish glob prok');
            expect(arabic).toEqual(24);
        });
        it('tegj glob pish => 59', () => {
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            const arabic = Translator.galacticPhraseToArabic('tegj glob pish');
            expect(arabic).toEqual(59);
        });
    });

    describe('elementsInPhrase() with glob, prok, pish, tegj', () => {
        it('pish tegj glob glob exists', () => {
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            expect(Translator.elementsInPhrase('pish tegj glob glob')).toBeTruthy();
        });
        it('pish exists', () => {
            Model.galacticRomanMap = { pish: 'X' };
            expect(Translator.elementsInPhrase('pish')).toBeTruthy();
        });
        it('dpish does not exist', () => {
            Model.galacticRomanMap = { pish: 'X' };
            expect(Translator.elementsInPhrase('dpish')).toBeFalsy();
        });
    });
});
