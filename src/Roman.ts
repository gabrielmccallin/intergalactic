export default class Roman {
    private static validationRegex = /^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/;
    private static map = [
        ['M', 1000],
        ['CM', 900],
        ['CD', 400],
        ['D', 500],
        ['C', 100],
        ['XC', 90],
        ['LXXX', 80],
        ['LXX', 70],
        ['LX', 60],
        ['L', 50],
        ['XL', 40],
        ['XXX', 30],
        ['XX', 20],
        ['X', 10],
        ['IX', 9],
        ['VIII', 8],
        ['VII', 7],
        ['VI', 6],
        ['V', 5],
        ['IV', 4],
        ['III', 3],
        ['II', 2],
        ['I', 1]
    ];

    private static romanToArabic(roman: string): number {
        let arabic = 0;
        while (roman.length > 0) {
            const found = Roman.map.filter(tuple => roman.indexOf(<string>tuple[0]) === 0)[0];

            const char = <string>found[0];
            arabic += <number>found[1];

            roman = roman.slice(char.length);

        }
        return arabic;
    };

    private static validate(roman): boolean {
        return (roman.search(Roman.validationRegex) !== -1) ? true : false;
    }

    static toArabic(roman: string): number {
        return Roman.validate(roman) ? Roman.romanToArabic(roman) : 0;
    }

}
