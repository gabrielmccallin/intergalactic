import Translator from "./Translator";
import Model from "./Model";

export default class ExtractDirectives {
    static processFile(input: string): string {
        Model.galacticRomanMap = {};

        return input
            .split('\n')
            .filter(statement => statement !== '')
            .map(statement => this.processDirective(statement))
            .filter(result => result !== '')
            .map(result => `${result}\n`)
            .toString()
            .replace(/\,/g, '')
            .slice(0, -1);
    }

    static processDirective(input: string): string {
        let output = input;
        if (input !== '') {
            if ((input.search(this.regex.credits) === -1) && (input.search(this.regex.question) === -1) && input.match(/\bis\b/)) {
                // match statement like 'glob is I'
                output = this.romanGalacticMapping(input);
            }
            if ((input.search(this.regex.credits) !== -1) && (input.search(this.regex.question) === -1)) {
                // match statement like 'glob glob Silver is 34 Credits'
                output = this.elementValueMapping(input);
            }
            if ((input.search(this.regex.credits) === -1) && (input.search(this.regex.question) !== -1)) {
                // match statement like 'how much woodchuck etc ?'
                output = this.answerGalacticToArabic(input);
            }
            if ((input.search(this.regex.credits) !== -1) && (input.search(this.regex.question) !== -1)) {
                // match statement like 'how many Credits is glob prok Silver ?'
                output = this.answerGalacticElementToCredits(input);
            }
        }

        return (output === input) ? Model.noIdea : output;
    }

    static romanGalacticMapping(input: string): string {
        const { galactic, numeral } = Translator.galacticToRoman(input);
        Model.galacticRomanMap[galactic] = numeral;
        return '';
    }

    static elementValueMapping(input: string): string {
        const { element, value } = Translator.setValueOfElement(input);
        Model.elementValueMap[element] = value;
        return '';
    }

    static answerGalacticToArabic(input: string): string {
        let output = input;
        if (Translator.elementsInPhrase(input)) {
            // match statement like 'how much is pish tegj glob glob ?'
            const galacticMatch = input.match(this.regex.howMuch);
            if (galacticMatch) {
                const galacticPhrase = galacticMatch[2];
                const arabic = Translator.galacticPhraseToArabic(galacticPhrase);

                output = `${galacticPhrase} is ${arabic}`;
            } else {
                output = '';
            }
        }
        return output;
    }

    static answerGalacticElementToCredits(input: string): string {
        let output = input;
        const galacticElement = input.match(this.regex.galacticElement);
        if (galacticElement) {

            const galacticPhrase = galacticElement[2];

            const { element, total } = Translator.elementTotal(galacticPhrase);
            const elementValue = Model.elementValueMap[element];

            output = `${galacticPhrase} is ${elementValue * total} Credits`;
        } else {
            output = '';
        }
        return output;
    }

    private static regex = {
        credits: /Credits/,
        howMuch: /(how much is )(.*)( \?)/,
        question: /\?$/,
        galacticElement: /(.*Credits is )(.*)( \?)$/
    }
}
