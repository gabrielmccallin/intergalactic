import Roman from "./Roman";
import Model from "./Model";

export default class Translator {
    static elementsInPhrase(input: string): boolean {
        const keys = Object.keys(Model.galacticRomanMap);
        const found = keys.filter(element => input.match(new RegExp(`\\b${element}\\b`)));
        return (found.length > 0) ? true : false;
    }

    static setValueOfElement(input: string): { element: string, value: number } {
        let element = '';
        let value = 0;

        const elementPhrase = input.match(this.regex.beforeIs)[0].slice(0, -3);
        if (elementPhrase) {
            const { element: elementTotal, total } = this.elementTotal(elementPhrase);
            element = elementTotal;

            const valueMatch = input.match(this.regex.creditsValue);
            if (valueMatch) value = (total !== 0) ? parseInt(valueMatch[2]) / total : 0;
        }

        return { element, value };
    }

    static galacticPhraseToArabic(phrase: string): number {
        let arabic = 0;
        const numerals = phrase.split(' ').map(galactic => Model.galacticRomanMap[galactic]);
        if (numerals.length > 0) arabic = Roman.toArabic(numerals.reduce((prev, curr) => prev + curr, ''));
        return arabic;
    }

    static galacticToRoman(input: string): { galactic: string, numeral: string } {
        let numeral = '';
        let galactic = '';

        const findIs = input.match(/\bis\b/);
        if (findIs) {
            numeral = input.slice(findIs.index + 3);
            galactic = input.slice(0, findIs.index - 1);
        }

        return { galactic, numeral };
    }

    static elementTotal(input: string): { element: string, total: number } {
        let element = '';
        let total = 0;

        const lastWord = input.match(/(\w+)$/);
        if (lastWord) {
            element = lastWord[0];
            const galacticPhrase = input.slice(0, lastWord.index - 1);
            total = Translator.galacticPhraseToArabic(galacticPhrase);
        }

        return { element, total }
    }

    private static regex = {
        creditsValue: /(.*is\s+)(.*)(\s+Credits.*)/,
        beforeIs: /^.*is/
    }
}
