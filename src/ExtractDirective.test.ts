import Roman from "./Roman";
import ExtractDirectives from "./ExtractDirectives";
import Model from "./Model";

describe('ExtractDirective', () => {
    describe('processFile()', () => {
        it('input set 1', () => {
            const processed = ExtractDirectives.processFile('glob is I\nprok is V\npish is X\ntegj is L\nglob glob Silver is 34 Credits\nglob prok Gold is 57800 Credits\npish pish Iron is 3910 Credits\nhow much is pish tegj glob glob ?\nhow many Credits is glob prok Silver ?\nhow many Credits is glob prok Gold ?\nhow many Credits is glob prok Iron ?\nhow much wood could a woodchuck chuck if a woodchuck could chuck wood ?');

            const expected = 'pish tegj glob glob is 42\nglob prok Silver is 68 Credits\nglob prok Gold is 57800 Credits\nglob prok Iron is 782 Credits\nI have no idea what you are talking about'

            expect(processed).toEqual(expected);
        });
        it('input set 2', () => {
            const processed = ExtractDirectives.processFile('anslrbvie is I\nyatraead is V\nlaksuthj is X\nmerythrj is L\nanslrbvie anslrbvie Nagata is 34 Credits\nanslrbvie yatraead Amos is 57800 Credits\nlaksuthj laksuthj Avasarala is 3910 Credits\nhow much is laksuthj merythrj anslrbvie anslrbvie ?\nhow many Credits is anslrbvie yatraead Nagata ?\nhow many Credits is anslrbvie yatraead Amos ?\nhow many Credits is anslrbvie yatraead Avasarala ?\nThe protomolecule turned an asteroid into a missile');

            const expected = 'laksuthj merythrj anslrbvie anslrbvie is 42\nanslrbvie yatraead Nagata is 68 Credits\nanslrbvie yatraead Amos is 57800 Credits\nanslrbvie yatraead Avasarala is 782 Credits\nI have no idea what you are talking about'

            expect(processed).toEqual(expected);
        });
    });

    describe('processDirective()', () => {
        it('glob is I => recognised roman to galactic directive', () => {
            const spy = jest.spyOn(ExtractDirectives, 'romanGalacticMapping');
            ExtractDirectives.processDirective('glob is I');

            expect(ExtractDirectives.romanGalacticMapping).toHaveBeenCalled();
        });
        it('pytasgdj is M => recognised roman to galactic directive', () => {
            const spy = jest.spyOn(ExtractDirectives, 'romanGalacticMapping');
            ExtractDirectives.processDirective('glob is I');

            expect(ExtractDirectives.romanGalacticMapping).toHaveBeenCalled();
        });
        it('glob glob Silver is 34 Credits, => recognised element to value directive', () => {
            const spy = jest.spyOn(ExtractDirectives, 'elementValueMapping');
            ExtractDirectives.processDirective('glob glob Silver is 34 Credits');

            expect(ExtractDirectives.elementValueMapping).toHaveBeenCalled();
        });
        it('asert ayrut bheu Silver is 34 Credits, => recognised element to value directive', () => {
            const spy = jest.spyOn(ExtractDirectives, 'elementValueMapping');
            ExtractDirectives.processDirective('asert ayrut bheu Silver is 34 Credits');

            expect(ExtractDirectives.elementValueMapping).toHaveBeenCalled();
        });
        it('bhuasrt liujet Helium is 27360 Credits => recognised element to value directive', () => {
            const spy = jest.spyOn(ExtractDirectives, 'elementValueMapping');
            ExtractDirectives.processDirective('bhuasrt liujet Helium is 27360 Credits');

            expect(ExtractDirectives.elementValueMapping).toHaveBeenCalled();
        });
        it('how much is pish tegj glob glob ? => 42', () => {
            const spy = jest.spyOn(ExtractDirectives, 'answerGalacticToArabic');
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            ExtractDirectives.processDirective('how much is pish tegj glob glob ?');

            expect(ExtractDirectives.answerGalacticToArabic).toHaveBeenCalled();
        });
        it('how much is irgiebgr ehkjbvps cnurobiha cnurobiha ? => 42', () => {
            const spy = jest.spyOn(ExtractDirectives, 'answerGalacticToArabic');
            Model.galacticRomanMap = { cnurobiha: 'I', prok: 'V', irgiebgr: 'X', ehkjbvps: 'L' };
            ExtractDirectives.processDirective('how much is irgiebgr ehkjbvps cnurobiha cnurobiha ?');

            expect(ExtractDirectives.answerGalacticToArabic).toHaveBeenCalled();
        });
        it('how many Credits is glob prok Silver ?', () => {
            const spy = jest.spyOn(ExtractDirectives, 'answerGalacticElementToCredits');
            Model.elementValueMap['Silver'] = 17;
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            ExtractDirectives.processDirective('how many Credits is glob prok Silver ?');

            expect(ExtractDirectives.answerGalacticElementToCredits).toHaveBeenCalled();
        });
        it('how many Credits is tegj pish prok glob glob Plutonium ?', () => {
            const spy = jest.spyOn(ExtractDirectives, 'answerGalacticElementToCredits');
            Model.elementValueMap['Plutonium'] = 456;
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            ExtractDirectives.processDirective('how many Credits is tegj pish prok glob glob Plutonium ?');

            expect(ExtractDirectives.answerGalacticElementToCredits).toHaveBeenCalled();
        });
        it('how much wood could a woodchuck chuck if a woodchuck could chuck wood ?', () => {
            const output = ExtractDirectives.processDirective('how much wood could a woodchuck chuck if a woodchuck could chuck wood ?');
            expect(output).toEqual(Model.noIdea);
        });
        it('Speak softly but carry a big stick', () => {
            const output = ExtractDirectives.processDirective('Speak softly but carry a big stick');
            expect(output).toEqual(Model.noIdea);
        });
    });

    describe('romanGalacticMapping()', () => {
        it('glob is I', () => {
            const output = ExtractDirectives.romanGalacticMapping('glob is I');

            expect(output).toEqual('');
            expect(Model.galacticRomanMap['glob']).toEqual('I');
        });
        it('pytasgdj is M', () => {
            const output = ExtractDirectives.romanGalacticMapping('pytasgdj is M');

            expect(output).toEqual('');
            expect(Model.galacticRomanMap['pytasgdj']).toEqual('M');
        });
    });

    describe('elementValueMapping()', () => {
        it('glob glob Silver is 34 Credits, => Silver = 17', () => {
            Model.galacticRomanMap = { glob: 'I' };
            Model.elementValueMap = {};
            const output = ExtractDirectives.elementValueMapping('glob glob Silver is 34 Credits');

            expect(output).toEqual('');
            expect(Model.elementValueMap['Silver']).toEqual(17);
        });
        it('asert ayrut bheu Silver is 34 Credits, => Silver = 0', () => {
            Model.galacticRomanMap = {};
            Model.elementValueMap = {};
            const output = ExtractDirectives.elementValueMapping('asert ayrut bheu Silver is 34 Credits');

            expect(output).toEqual('');
            expect(Model.elementValueMap['Silver']).toEqual(0);
        });
        it('bhuasrt liujet Helium is 27360 Credits => Helium = 684', () => {
            Model.galacticRomanMap = { bhuasrt: 'X', liujet: 'L' };
            Model.elementValueMap = {};
            const output = ExtractDirectives.elementValueMapping('bhuasrt liujet Helium is 27360 Credits');

            expect(output).toEqual('');
            expect(Model.elementValueMap['Helium']).toEqual(684);
        });
    });

    describe('answerGalacticToArabic()', () => {
        it('how much is pish tegj glob glob ? => 42', () => {
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            const output = ExtractDirectives.answerGalacticToArabic('how much is pish tegj glob glob ?');

            expect(output).toEqual('pish tegj glob glob is 42');
        });
        it('how much is irgiebgr ehkjbvps cnurobiha cnurobiha ? => 42', () => {
            Model.galacticRomanMap = { cnurobiha: 'I', prok: 'V', irgiebgr: 'X', ehkjbvps: 'L' };
            const output = ExtractDirectives.answerGalacticToArabic('how much is irgiebgr ehkjbvps cnurobiha cnurobiha ?');

            expect(output).toEqual('irgiebgr ehkjbvps cnurobiha cnurobiha is 42');
        });
    });

    describe('answerGalacticElementToCredits()', () => {
        it('how many Credits is glob prok Silver ?', () => {
            Model.elementValueMap['Silver'] = 17;
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            const output = ExtractDirectives.answerGalacticElementToCredits('how many Credits is glob prok Silver ?');

            expect(output).toEqual('glob prok Silver is 68 Credits');
        });
        it('how many Credits is tegj pish prok glob glob Plutonium ?', () => {
            Model.elementValueMap['Plutonium'] = 456;
            Model.galacticRomanMap = { glob: 'I', prok: 'V', pish: 'X', tegj: 'L' };
            const output = ExtractDirectives.answerGalacticElementToCredits('how many Credits is tegj pish prok glob glob Plutonium ?');

            const value = 456 * 67;
            expect(output).toEqual(`tegj pish prok glob glob Plutonium is ${value} Credits`);
        });
    });
});
