# Merchant's Guide to the Galaxy 👽

## Requirements
nodejs (https://nodejs.org/) version 6.4.0 and above

## Build
(Build is required before running the application or tests).
- `cd` into `intergalactic` folder
- `npm install`
- `npm run build`

## Run
- `cd` into `intergalactic` folder
- `npm start -- /path/to/input/file`  
(There are two input files at the root of the project. If a file is not supplied the application will load `test-input.txt` by default)

## Tests
- `cd` into `intergalactic` folder
- `npm run test`

Enjoy! 😃

## Design  
This application processes statements in order to derive:
- the value of galactic units in roman numerals (e.g. `glob is I`)
- using the galactic to roman mapping, the value of elements in Credits (e.g. `glob glob Silver is 34 Credits`)

It is then able to answer these questions:
- Value of galactic phrases in arabic numbers? (e.g. `how much is pish tegj glob glob ?`)
- Value in Credits of an element with a galactic multiplier? (e.g. `how many Credits is glob prok Iron ?`)
- Any questions it does not understand it will return `I have no idea what you are talking about`;

The program assumes that the first lines of input will provide the galactic to roman mapping and the element to credit mapping. Only then can it answer the questions.

Test input:
```
glob is I
prok is V
pish is X
tegj is L
glob glob Silver is 34 Credits
glob prok Gold is 57800 Credits
pish pish Iron is 3910 Credits
how much is pish tegj glob glob ?
how many Credits is glob prok Silver ?
how many Credits is glob prok Gold ?
how many Credits is glob prok Iron ?
how much wood could a woodchuck chuck if a woodchuck could chuck wood ?
```

Test output:
```
pish tegj glob glob is 42
glob prok Silver is 68 Credits
glob prok Gold is 57800 Credits
glob prok Iron is 782 Credits
I have no idea what you are talking about
```
